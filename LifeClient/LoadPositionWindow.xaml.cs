﻿using System;
using System.Collections.Generic;
using LifeClient.ServiceLife;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LifeClient
{
    /// <summary>
    /// Форма загрузки позиции из БД
    /// </summary>
    public partial class LoadPositionWindow : Window
    {
        /// <summary>
        /// Ссылка на сервер
        /// </summary>
        private ServiceLifeClient client;

        /// <summary>
        /// Делегат на функцию из родительской формы для загрузки позиции
        /// </summary>
        private ChangeGame cg;

        /// <summary>
        /// Список позиций из БД, которые можно загрузить
        /// </summary>
        private List<PositionModel> positions;

        /// <summary>
        /// Инициализация формы
        /// </summary>
        /// <param name="client">Ссылка на сервер</param>
        /// <param name="endless">Признак бесконечности</param>
        /// <param name="lp">Делегат на функцию из родительской формы для загрузки позиции</param>
        public LoadPositionWindow(ServiceLifeClient client, bool endless, ChangeGame cg)
        {
            this.client = client;
            this.cg = cg;
            InitializeComponent();
            positions = new List<PositionModel>(client.PositionLoad());
            dgLoadGame.ItemsSource = positions;
        }

        /// <summary>
        /// Кнопка загрузить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            PositionModel pm = dgLoadGame.SelectedItem as PositionModel;
            cg(pm.GameField, pm.GameField, pm.Height, pm.Width, DateTime.Now, DateTime.Now);
            this.Close();
        }

        /// <summary>
        /// Кнопка отменить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Формирование столбцов DataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgLoadGame_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "ExtensionData")
            {
                e.Cancel = true;
            }
            if (e.Column.Header.ToString() == "GameField")
            {
                e.Cancel = true;
            }
            if (e.Column.Header.ToString() == "Id")
            {
                e.Column.DisplayIndex = 0;
            }
            if (e.Column.Header.ToString() == "Height")
            {
                e.Column.DisplayIndex = 1;
            }
            if (e.Column.Header.ToString() == "Width")
            {
                e.Column.DisplayIndex = 2;
            }
            if (e.Column.Header.ToString() == "SaveTime")
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "dd/MM/yyyy HH:mm:ss"; 
                e.Column.DisplayIndex = 3;
            }
        }

        /// <summary>
        /// Двойной клик на списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgLoadGame_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PositionModel pm = dgLoadGame.SelectedItem as PositionModel;
            cg(pm.GameField, pm.GameField, pm.Height, pm.Width, DateTime.Now, DateTime.Now);
            this.Close();
        }

        /// <summary>
        /// Кнопка загрузки случайно позиции из списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRandom_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            PositionModel pm = dgLoadGame.Items[rnd.Next(0, dgLoadGame.Items.Count - 1)] as PositionModel;
            cg(pm.GameField, pm.GameField, pm.Height, pm.Width, DateTime.Now, DateTime.Now);
            this.Close();
        }

        /// <summary>
        /// Кнопка удаления позиции из БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure?",
                                          "Confirmation",
                                          MessageBoxButton.YesNo,
                                          MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                return;
            }
            PositionModel pm = dgLoadGame.SelectedItem as PositionModel;
            client.PositionDelete(pm.Id);
            positions.Remove(pm);
            dgLoadGame.Items.Refresh();
        }
    }
}
