﻿using System;
using System.Collections.Generic;
using System.Linq;
using LifeClient.ServiceLife;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace LifeClient
{
    /// <summary>
    /// Делегат на функцию загрузки игры
    /// </summary>
    /// <param name="startField">Начальное поле</param>
    /// <param name="field">Текущее поле</param>
    /// <param name="gameStart">Время начала игры</param>
    public delegate void ChangeGame(bool[] field, bool[] startField, int height, int width, DateTime gameStart, DateTime gameEnd);

    /// <summary>
    /// Главное окно
    /// </summary>
    public partial class MainWindow : Window, IServiceLifeCallback
    {
        /// <summary>
        /// Ссылка на цвет для закраски мертвых клеток
        /// </summary>
        private readonly SolidColorBrush deadBrush = Brushes.Black;

        /// <summary>
        /// Ссылка на цвет для закраски живых клеток
        /// </summary>
        private readonly SolidColorBrush lifeBrush = Brushes.Green;

        /// <summary>
        /// Высота игрового поля
        /// </summary>
        private int height = 25;

        /// <summary>
        /// Ширина игрового поля
        /// </summary>
        private int width = 25;

        /// <summary>
        /// Таймер следующего хода
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// Время окончания игры
        /// </summary>
        private DateTime gameEnd;

        /// <summary>
        /// Время начала игры
        /// </summary>
        private DateTime gameStart;

        /// <summary>
        /// Ссылка на сервер
        /// </summary>
        private ServiceLifeClient client;

        /// <summary>
        /// Признак ручного изменения позиции (начала новой игры)
        /// </summary>
        private bool positionIsChanged = false;

        /// <summary>
        /// Стартовая позиция
        /// </summary>
        internal bool[] StartField { get; set; }

        /// <summary>
        /// Текущая позиция
        /// </summary>
        internal bool[] Field { get; set; }

        /// <summary>
        /// Текущая позиция в графическом представлении
        /// </summary>
        internal Rectangle[] Rectangles { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Инициализация формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            gameStart = DateTime.Now;
            gameEnd = DateTime.Now;
            Field = new bool[height * width];
            Rectangles = new Rectangle[height * width];
            ClearField();
            client = new ServiceLifeClient(new System.ServiceModel.InstanceContext(this));
            client.NewGame(Field, height, width, false);
            Draw();
            SetNewGame();
        }

        /// <summary>
        /// Кнопка следущий ход
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            RectToField();
            client.NewGame(Field, height, width, cboxEndless.IsChecked.Value);
            NextTurn();
        }

        /// <summary>
        /// Логика выполнения хода
        /// </summary>
        private void NextTurn()
        {
            gameEnd = DateTime.Now;
            if (client.NextTurn(cboxTwoTurnsChek.IsChecked.Value) == 1)
            {
                timer.Stop();
                EnableDisableAll(true);
                btnPlay.Content = "Play";
                return;
            }
            Field = client.Display();
            Draw();
        }

        /// <summary>
        /// Рисование
        /// </summary>
        public void Draw()
        {
            fieldView.Children.Clear();
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Rectangle r = new Rectangle
                    {
                        Width = fieldView.ActualWidth / width - 2.0,
                        Height = fieldView.ActualHeight / height - 2.0,
                        Fill = (Field[i * width + j] == true) ? lifeBrush : deadBrush
                    };
                    fieldView.Children.Add(r);
                    Canvas.SetLeft(r, j * fieldView.ActualWidth / width);
                    Canvas.SetTop(r, i * fieldView.ActualHeight / height);
                    r.MouseDown += R_MouseDown;
                    Rectangles[i * width + j] = r;
                }
            }
        }

        /// <summary>
        /// Обработка нажатия на прямоугольниках
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void R_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ((Rectangle)sender).Fill = (((Rectangle)sender).Fill == lifeBrush) ? deadBrush : lifeBrush;
            if (!positionIsChanged)
            {
                positionIsChanged = true;
            }
        }

        /// <summary>
        /// Заполение поля пустыми клетками (сброс позиции)
        /// </summary>
        private void ClearField()
        {
            for (int i = 0; i < Field.GetLength(0); i++)
            {
                Field[i] = false;
            }
        }

        /// <summary>
        /// Заполнение поля сулчайным образом
        /// </summary>
        private void FieldRandom()
        {
            Random rnd = new Random();
            for (int i = 0; i < Field.GetLength(0); i++)
            {
                Field[i] = (rnd.Next(0,5) == 1) ? true : false;
            }
        }

        /// <summary>
        /// Преобразование поля из графического представления в логическое
        /// </summary>
        private void RectToField()
        {
            for (int i = 0; i < Rectangles.GetLength(0); i++)
            {
                Field[i] = (Rectangles[i].Fill == lifeBrush) ? true : false;
            }
        }

        /// <summary>
        /// Кнопка начала игры в автоматическом режиме по таймеру
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (timer.IsEnabled)
            {
                timer.Stop();
                gameEnd = DateTime.Now;
                EnableDisableAll(true);
                btnPlay.Content = "Play";
            }
            else
            {
                RectToField();
                if ((txtHeight.Text.ToString() != height.ToString()) || (txtWidth.Text.ToString() != width.ToString()))
                {
                    Resize();
                }
                if (FieldIsEmpty(Field))
                {
                    FieldRandom();
                }
                client.NewGame(Field, height, width, cboxEndless.IsChecked.Value);
                timer.Interval = TimeSpan.FromSeconds(1 / slSpeed.Value);
                timer.Tick += Timer_Tick;
                timer.Start();
                EnableDisableAll(false);
                btnPlay.Content = "Pause";
                if (positionIsChanged)
                {
                    SetNewGame();
                }
            }
        }

        /// <summary>
        /// Закрытие/Открытие компонентов программы дял пользователя
        /// </summary>
        /// <param name="enable">Признак доступности</param>
        private void EnableDisableAll(bool enable)
        {
            cboxEndless.IsEnabled = enable;
            btnNext.IsEnabled = enable;
            btnClear.IsEnabled = enable;
            btnRandom.IsEnabled = enable;
            cboxTwoTurnsChek.IsEnabled = enable;
            btnSave.IsEnabled = enable;
            btnLoad.IsEnabled = enable;
            menu.IsEnabled = enable;
        }

        /// <summary>
        /// Обработка события таймера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            NextTurn();
        }

        /// <summary>
        /// Слайдер настройки скорости автоматического воспроизведения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            timer.Interval = TimeSpan.FromSeconds(1 / slSpeed.Value);
        }

        /// <summary>
        /// Кнопка очистки поля
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearField();
            client.NewGame(Field, height, width, false);
            Draw();
        }

        /// <summary>
        /// Кнопка случайной расстановки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRandom_Click(object sender, RoutedEventArgs e)
        {
            FieldRandom();
            client.NewGame(Field, height, width, false);
            Draw();
            SetNewGame();
        }

        /// <summary>
        /// Кнопка сохранения состояния текущей игры в БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            RectToField();
            client.GameSave(StartField, Field, height, width, gameStart, gameEnd);
            MessageBoxResult result = MessageBox.Show("Game was succesfully saved",
                              "Saved",
                              MessageBoxButton.OK,
                              MessageBoxImage.Information);
        }

        /// <summary>
        /// Кнопка загрузки игры из БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadGameWindow loadGameWindow = new LoadGameWindow(client, cboxEndless.IsChecked.Value, new ChangeGame(ChangeGame));
            loadGameWindow.Owner = this;
            loadGameWindow.ShowDialog();
            Draw();
        }

        /// <summary>
        /// Проверяет является ли поле полностью "мертвым"
        /// </summary>
        /// <param name="field">Поле дял проверки</param>
        /// <returns></returns>
        private bool FieldIsEmpty(bool[] field)
        {
            bool empty = true;
            foreach(bool f in field)
            {
                if (f == true)
                {
                    empty = false;
                    break;
                }
            }
            return empty;
        }

        /// <summary>
        /// Изменяет размеры поля
        /// </summary>
        private void Resize()
        {
            int h = 0;
            int w = 0;
            if (int.TryParse(txtHeight.Text, out h) && int.TryParse(txtWidth.Text, out w))
            {
                ChangeGame(new bool[h*w], new bool[h*w], h, w, DateTime.Now, DateTime.Now);
            }
        }

        /// <summary>
        /// Кнопка меню загрузку позиции из БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadPositionWindow loadPositionWindow = new LoadPositionWindow(client, cboxEndless.IsChecked.Value, new ChangeGame(ChangeGame));
            loadPositionWindow.Owner = this;
            loadPositionWindow.ShowDialog();
            Draw();
            SetNewGame();
        }

        /// <summary>
        /// Кнопка меню сохранения позиции в БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuSave_Click(object sender, RoutedEventArgs e)
        {
            RectToField();
            if (client.PositionSave(Field, height, width) == 0)
            {
                MessageBoxResult result = MessageBox.Show("Position was succesfully saved",
                  "Saved",
                  MessageBoxButton.OK,
                  MessageBoxImage.Information);
            }
            else
            {
                 MessageBoxResult result = MessageBox.Show("This position already exists",
                 "Position exists",
                 MessageBoxButton.OK,
                 MessageBoxImage.Information);
            }

        }

        /// <summary>
        /// Делает текущую игру "новой"
        /// </summary>
        private void SetNewGame()
        {
            gameStart = DateTime.Now;
            StartField = new bool[height * width];
            for (int i = 0; i < height * width; i++)
            {
                StartField[i] = Field[i];
            }
            positionIsChanged = false;
        }

        /// <summary>
        /// Смена контекста игры
        /// </summary>
        /// <param name="field">поле</param>
        /// <param name="startField">стартовое поле</param>
        /// <param name="height">высота</param>
        /// <param name="width">ширина</param>
        /// <param name="gameStart">время начала</param>
        private void ChangeGame(bool[] field, bool[] startField, int height, int width, DateTime gameStart, DateTime gameEnd)
        {
            txtHeight.Text = height.ToString();
            txtWidth.Text = width.ToString();
            this.height = height;
            this.width = width;
            this.gameStart = gameStart;
            this.gameEnd = gameEnd;
            StartField = new bool[height * width];
            Field = new bool[height * width];
            Rectangles = new Rectangle[height * width];
            for (int i = 0; i < Field.GetLength(0); i++)
            {
                StartField[i] = startField[i];
                Field[i] = field[i];
            }
            client.NewGame(Field, height, width, cboxEndless.IsChecked.Value);
            Draw();
            positionIsChanged = false;
        }

        public void MsgCallback(string msg)
        {
            throw new NotImplementedException();
        }
    }
}
