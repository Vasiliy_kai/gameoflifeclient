﻿using System;
using System.Collections.Generic;
using LifeClient.ServiceLife;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LifeClient
{
    /// <summary>
    /// Форма загрузки игры
    /// </summary>
    public partial class LoadGameWindow : Window
    {
        /// <summary>
        /// Ссылка на сервер
        /// </summary>
        private ServiceLifeClient client;

        /// <summary>
        /// Делегат на функцию из родительской формы для загрузки игры
        /// </summary>
        private ChangeGame cg;

        /// <summary>
        /// Список игр из БД, которые можно загрузить
        /// </summary>
        private List<GameModel> games;

        /// <summary>
        /// Инициализация формы
        /// </summary>
        /// <param name="client">Ссылка на сервер</param>
        /// <param name="endless">Признак бесконечности</param>
        /// <param name="lg">Делегат на функцию загрузки</param>
        public LoadGameWindow(ServiceLifeClient client, bool endless, ChangeGame cg)
        {
            this.client = client;
            this.cg = cg;
            InitializeComponent();
            games = new List<GameModel> (client.GameLoad());
            dgLoadGame.ItemsSource = games;
        }

        /// <summary>
        /// Кнопка загрузить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            GameModel gm = dgLoadGame.SelectedItem as GameModel;
            cg(gm.StartField, gm.GameField,gm.Height, gm.Width, gm.GameStart, gm.GameEnd);
            this.Close();
        }

        /// <summary>
        /// Кнопка отменить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Формирование столбцов DataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgLoadGame_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "ExtensionData")
            {
                e.Cancel = true;
            }
            if (e.Column.Header.ToString() == "GameField")
            {
                e.Cancel = true;
            }
            if (e.Column.Header.ToString() == "StartField")
            {
                e.Cancel = true;
            }
            if (e.Column.Header.ToString() == "Id")
            {
                e.Column.DisplayIndex = 0;
            }
            if (e.Column.Header.ToString() == "GameStart")
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "dd/MM/yyyy HH:mm:ss";               
                e.Column.DisplayIndex = 1;
            }
            if (e.Column.Header.ToString() == "GameEnd")
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "dd/MM/yyyy HH:mm:ss";
                e.Column.DisplayIndex = 2;
            }
            if (e.Column.Header.ToString() == "Height")
            {
                e.Column.DisplayIndex = 3;
            }
            if (e.Column.Header.ToString() == "Width")
            {
                e.Column.DisplayIndex = 4;
            }
        }

        /// <summary>
        /// Двойной клик по списку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgLoadGame_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GameModel gm = dgLoadGame.SelectedItem as GameModel;
            cg(gm.StartField, gm.GameField,gm.Height, gm.Width, gm.GameStart, gm.GameEnd);
            this.Close();
        }

        /// <summary>
        /// Кнопка случайная игра
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRandom_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            GameModel gm = dgLoadGame.Items[rnd.Next(0, dgLoadGame.Items.Count - 1)] as GameModel;
            cg(gm.StartField, gm.GameField, gm.Height, gm.Width, gm.GameStart, gm.GameEnd);
            this.Close();
        }

        /// <summary>
        /// Кнопка удалить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure?",
                                          "Confirmation",
                                          MessageBoxButton.YesNo,
                                          MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                return;
            }
            GameModel gm = dgLoadGame.SelectedItem as GameModel;
            client.GameDelete(gm.Id);
            games.Remove(gm);
            dgLoadGame.Items.Refresh();
        }
    }
}
